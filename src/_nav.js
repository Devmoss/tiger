import React from "react"
    import CIcon from "@coreui/icons-react"
    
    const _nav = [
    {
    title: true,
    component: "CNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon name="cil-speedometer" customClassName="nav-icon" />,
    attributes: { title: "Dashboard tooltip" }
    },
     {
            component: "CNavItem",
            name: "ข้อมูลลูกค้า",
            to: "/customers",
            icon: <CIcon name="cil-notes" customClassName="nav-icon" />,
            },
             {
            component: "CNavItem",
            name: "ข้อมูลtest",
            to: "/test",
            icon: <CIcon name="cil-notes" customClassName="nav-icon" />,
            },
            ]
    
    export default _nav
    