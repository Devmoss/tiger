import * as React from "react";
    import { Component } from "react";
    import {
    DataGrid,
    GridToolbarContainer,
    GridToolbarExport,
    } from "@mui/x-data-grid";
    import {
    CAvatar,
    CAlert,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CTable,
    CTableBody,
    CTableDataCell,
    CTableHead,
    CTableHeaderCell,
    CTableRow,
    CListGroup,
    CListGroupItem,
    CFormSwitch,
    CForm,
    CFormLabel,
    CFormInput,
    CFormTextarea,
    CModal,
    CTooltip,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    } from "@coreui/react";
    import axios from "axios";
    import CIcon from "@coreui/icons-react";
    import Select from "react-select";
    import { API_URL } from "../../settings";
    import {
    getCurDate,
    escapeRegExp
    } from "../../lib/util"
    import LoadingData from "../../containers/LoadingData"
    import ModalViewContent from "../../containers/ModalViewContent"
    import ConfirmDialog from "../../containers/ConfirmDialog"
    import { TestForm } from "./TestForm"
    import "./test.css"
    
    export class TestList extends Component {
    constructor(props) {
    const CUR_DATE = getCurDate()
    super(props);
    this.state = {
    curDate: CUR_DATE,
    isLoaded: false,
    isDataEmpty: false,
    isShowCreate: false,
    isShowEdit: false,
    isPopupView: true,
    resultData: [],
    rowData: [],
    filter: [],
    columnData: [],
    penddingChange: false,
    rowDataEdit: "",
    titleList: "ข้อมูลtest",
    titleCreateData: "เพิ่มข้อมูลtest",
    titleEditData: "แก้ไขข้อมูลtest"
    };
    }
    
    componentDidMount = async () => {
    await this.reloadData()
    await this.setColumnData()
    
    };
    
    reloadData = async (search) => {
    var filter = this.getFilterData(this.state.filter)
    try {
    let results = await axios.get(API_URL + "test" + "?search=" + (search != undefined ? search : "")
    + "&search_criteria=" + filter
    )
    if (results.data.length <= 0) {
    this.setState({
    isLoaded: true,
    isDataEmpty: true,
    rowData: [],
    resultData: [],
    })
    } else {
    let rowDataEdit = await this.state.rowDataEdit
    if (rowDataEdit) {
    rowDataEdit = await results.data.find(data =>
    data.id == this.state.rowDataEdit.id
    )
    }
    this.setState({
    isDataEmpty: false,
    resultData: results.data,
    isLoaded: true,
    rowData: results.data,
    rowDataEdit: rowDataEdit
    })
    }
    
    } catch (error) {
    this.setState({ isLoaded: true, isDataEmpty: true });
    console.log("error message : " + error)
    }
    }
    
    getFilterData = (filter) => {
        var filterData = ""
        filter.map(data =>
        (
            filterData += data + ","
        )
        )
        return filterData.substring(0, filterData.length - 1)
    }
    
    setColumnData = () => {
    //Other features pls read https://material-ui.com/components/data-grid
    var columns = [
    { //Add Culumn ID and hide it.
    field: "id", hide: true
    },
    
    {//Add Culumn button Edit and Remove
    field: " ",
    flex: 1,
    sortable: false,
    disableClickEventBubbling: true,
    renderCell: (params) => {
    const onClickEdit = () => {
    this.handleEditData(params.row);
    };
    const onClickRemove = () => {
    this.handleRemoveData(params.row);
    };
    return (
    <>
    <CButton
    color="secondary"
    variant="outline"
    style={{
    borderRadius: 10
    }}
    onClick={onClickEdit}
    >
    <CIcon icon={"cil-pencil"} />
    </CButton>
    <CButton
    color="danger"
    variant="outline"
    style={{
    marginLeft: 5,
    borderRadius: 10
    }}
    onClick={onClickRemove}
    >
    <CIcon icon={"cil-trash"} />
    </CButton>
    </>
    );
    },
    },
    ];
    this.setState({ columnData: columns })
    }
    
    handleSearchChange = () => (e) => {
        if (e.keyCode == 13) {
            var searchValue = e.target.value;
            this.reloadData(searchValue)
        }
    };
    
    handleCreateData() {
    this.setState({
    isShowCreate: true,
    isShowEdit: false
    })
    }
    
    handleEditData(rowDataEdit) {
    this.setState({
    rowDataEdit: rowDataEdit,
    isShowEdit: true,
    isShowCreate: false
    })
    }
    
    handleRemoveData = async (data) => {
    if (window.confirm("คุณยืนยันที่จะลบใช่หรือไม่?")) {
    await axios.delete(API_URL + "test/" + data.id);
    this.reloadData()
    }
    }
    
    handleCloseCreate = () => {
    this.setState({ isShowCreate: false })
    }
    
    handleCloseEdit = () => {
    this.setState({ isShowEdit: false })
    }
    
    handleChangViewForm = () => {
    if (this.state.isPopupView) this.setState({ isPopupView: false })
    else this.setState({ isPopupView: true })
    }
    
    //change state field for multiple select field form
    handleChangMultiOption = (input) => (e) => {
        if (e) {
            this.setState({
                [input]: e.map(data => data.value),
            });
        } else {
            this.setState({
                [input]: [],
            });
        }
    }
    
    render() {
    const {
    rowData,
    columnData,
    filter,
    isLoaded,
    isDataEmpty,
    isShowCreate,
    isShowEdit,
    rowDataEdit,
    isPopupView,
    titleList,
    titleCreateData,
    titleEditData,
    isShowExitDialog
    } = this.state;
    
    const optionSearchable = [
        { label: "ชื่อ", value: "name" },
{ label: "นามสกุล", value: "lname" },

    ]
    
    return (
    <>
    <CCard className="mb-4">
    <CCardHeader>
    <CRow>
    <CCol>
    <h4>{titleList}</h4>
    </CCol>
    </CRow>
    
    </CCardHeader>
    <CCardBody style={{ paddingBottom: 5 }}>
    <CRow>
    <CCol sm="4">
    <CFormInput
    type="text"
    id="searchData"
    placeholder="ค้นหา..."
    onKeyDown={this.handleSearchChange()}
    />
    </CCol>
    <CCol sm="4">
    <Select
    className = "basic-single"
    classNamePrefix = "select"
    isClearable = { true}
    isMulti = { true}
    options = { optionSearchable }
    defaultMultiValue = { filter }
    onChange = { this.handleChangMultiOption("filter") }
    placeholder = { "เงื่อนไขการค้นหา..."}
        />
    </CCol >
    <CCol sm="4" style={{ textAlign: "right" }}>
        <CButton className="bg-info float-right "
        onClick={() => this.handleCreateData()}>
        <img height="25" src={require("../../assets/icons/icon_plus.png").default} />
        <b style={{ fontSize: "0.75rem" }}>{titleCreateData}</b>
    </CButton>
    </CCol >
    </CRow >
    <CRow style={{ margin: 0 }}>
        {(isLoaded && !isDataEmpty) &&
        <CCol style={{ marginTop: 20, padding: 0 }}>
            <DataGrid
            rows={rowData}
            columns={columnData}
            pageSize={10}
            rowsPerPageOptions={[5]}
            checkboxSelection={false}
            disableColumnFilter
            disableSelectionOnClick
            autoHeight={true}
            onRowClick={(event) => (isShowCreate || isShowEdit) ? (this.handleEditData(event.row)) : ""}
    />
        </CCol>
    }
        {/* ===== Section Loading data ====== */}
        <LoadingData isLoaded={isLoaded} isDataEmpty={isDataEmpty} />
    </CRow>
    </CCardBody >
    </CCard >
    
    {/* ===== Section Modal View Form ====== */ } 
    <ModalViewContent n        show={isShowCreate || isShowEdit} n        showDialogExit={true} n        title={isShowCreate ? titleCreateData : titleEditData} n        isPopupView={isPopupView} n        onChangeViewForm={this.handleChangViewForm} n        onClose={isShowCreate ? this.handleCloseCreate : this.handleCloseEdit}>
        <TestForm isEdit={isShowEdit} rowData={rowDataEdit} reloadData={this.reloadData}  isPopupView={isPopupView}/>
    </ModalViewContent>
    
    </>
    ) 
} 
    }
export default TestList