import React, { lazy } from "react"
    import {
    CAvatar,
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CTable,
    CTableBody,
    CTableDataCell,
    CTableHead,
    CTableHeaderCell,
    CTableRow,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CModalTitle,
    CForm,
    CFormLabel,
    CFormInput,
    CFormTextarea,
    CFormCheck
    } from "@coreui/react"
    //base on react select component
    //other fetures pls read https://react-select.com/
    import Select from "../../containers/SelectOption";
    //=================================================
    import StepperForm from "../../containers/StepperForm"
    import ImageUploading from "react-images-uploading";
    import SnackbarAlert from "../../containers/SnackbarAlert"
    import AsteriskPoint from "../../containers/AsteriskPoint";
    import DateInput from "../../containers/DateInput"
    import CIcon from "@coreui/icons-react"
    import axios from "axios";
    import { Component } from "react"
    import {
    API_URL
    } from "../../settings"
    import {
    getCurDate
    } from "../../lib/util"
    
    export class CustomersForm extends Component {
    constructor(props) {
    super(props);
    this.state = {
    curDate: getCurDate(),
    resultData: [],
    isEdit: false,
    colWidth: "",
    rowData: "",
    isShowAlert: false,
    colorAlert: "",
    messageAlert: "",
    amountStep: 2,
    stepForm: 0,
    isLastSection: false,
    id: "",
    createAt: "",
    updateAt: "",
    //Field
    personal_img: [],
first_name: "",
last_name: "",
birth_date: "",
have_car_cer: false,
married_status: "",
}
    }
    
    componentDidMount = async () => {
    //set full width collumn for sidebarview
    if (!this.props.isPopupView) {
    this.setState({ colWidth: "col-sm-12" })
    }
    this.reloadData()
    }
    
    reloadData = async () => {
    if (this.props.rowData && this.props.isEdit) {
    var rowData = this.props.rowData
    this.setState({
    id: rowData.id,
    createAt: rowData.creat_at,
    updateAt: rowData.update_at,
    //setStateFieldsFromProps ***
    personal_img: rowData.personal_img,
first_name: rowData.first_name,
last_name: rowData.last_name,
birth_date: rowData.birth_date,
have_car_cer: rowData.have_car_cer,
married_status: rowData.married_status,
})
    }
    //setLookupData ***
    this.setState({ isEdit: this.props.isEdit })
    }
    //resetDataForm ***
    resetData = () => {
    this.setState({
    id: "",
    createAt: "",
    updateAt: "",
    personal_img: [],
first_name: "",
last_name: "",
birth_date: "",
have_car_cer: false,
married_status: "",
})
    }
    //reset data when props change for view data after submit sidebar form
    componentWillReceiveProps = async (nextProps) => {
    await this.resetData()
    this.reloadData()
    }
    
    //change state field for general field form
    inputChange = (input) => (e) => {
    this.setState({
    [input]: e.target.value,
    });
    };
    
    //change date input 
    inputChangeDate = (input) => (e) => {
    this.setState({
    [input]: e.format("YYYY/MM/DD"),
    });
    };
    
    //change state field for checkbox field form
    inputCheck = (input) => (e) => {
    this.setState({
    [input]: e.target.checked,
    });
    };
    
    //change state field for single select field form
    handleChangeSingleOption = (input) => (e) => {
    if (e) {
    this.filterLookupData(input, e.value)
    this.setState({
    [input]: e.value,
    });
    } else {
    this.setState({
    [input]: "",
    });
    }
    }
    
    //change state field for multiple select field form
    handleChangMultiOption = (input) => (e) => {
    if (e) {
    this.setState({
    [input]: e.map(data => data.value),
    });
    } else {
    this.setState({
    [input]: [],
    });
    }
    }
    
    //get data for lookup entity
    setLookupDataFromEntity = async (fieldOptionName, entityLookupName, lookUpLable, lookUpValue, filterBy, filterData) => {
    try {
    let results = await axios.get(API_URL + entityLookupName);
    if (!filterData) {
    this.setState({
    [fieldOptionName]: results.data.map(data =>
    ({
    "label": data[lookUpLable],
    "value": data[lookUpValue],
    })
    )
    })
    } else {
    this.setState({
    [fieldOptionName]: results.data.filter(
    data => data[filterBy] == filterData
    ).map(data =>
    ({
    "label": data[lookUpLable],
    "value": data[lookUpValue],
    })
    )
    })
    }
    }
    catch (e) {
    console.log("message : " + e)
    }
    };
    
    //filterLookupDATA ***
    filterLookupData = async (fieldAffect, filterData) => {
    switch (fieldAffect) {
    
    
    default:
    break;
    }
    }
    
    handleOnChangeInputImage = (input) => (imageList, addUpdateIndex) => {
        this.setState({ [input]: imageList });
    };
    
    //Action When submit Form ***
    handleSubmitForm = () => {
    const {
    curDate,
    isEdit,
    id,
create_at,
update_at,
personal_img,
first_name,
last_name,
birth_date,
have_car_cer,
married_status,
} = this.state
    if (isEdit) {
    axios({
    method: "PUT",
    url: API_URL + "customers/" + id,
    headers: {
    "Content-Type": "application/json",
    },
    data: {
    "update_at": curDate,
    //setFields ***
    "personal_img": personal_img,
"first_name": first_name,
"last_name": last_name,
"birth_date": birth_date,
"have_car_cer": have_car_cer,
"married_status": married_status,
},
    })
    .then((result) => {
    this.setState({
    isShowAlert: true,
    colorAlert: "success",
    messageAlert: "แก้ไขสำเร็จ",
    penddingChange: false,
    })
    this.props.reloadData();
    }, (error) => {
    this.setState({
    isShowAlert: true,
    colorAlert: "error",
    messageAlert: "ไม่สำเร็จ"
    })
    }
    )
    } else {
    axios({
    method: "POST",
    url: API_URL + "customers",
    headers: {
    "Content-Type": "application/json",
    },
    data: {
    "create_at": curDate,
    "update_at": curDate,
    //setFields ***
    "personal_img": personal_img,
"first_name": first_name,
"last_name": last_name,
"birth_date": birth_date,
"have_car_cer": have_car_cer,
"married_status": married_status,
},
    })
    .then((result) => {
    this.setState({
    isShowAlert: true,
    colorAlert: "success",
    messageAlert: "บันทึกสำเร็จ",
    penddingChange: false,
    isSubmitForm: true
    })
    this.props.reloadData();
    }, (error) => {
    this.setState({
    isShowAlert: true,
    colorAlert: "error",
    messageAlert: "ไม่สำเร็จ"
    })
    }
    )
    }
    }
    
    handleCloseAlert = () => {
    this.setState({ isShowAlert: false })
    }
    
    handleClickButtonForm = (buttonName) => {
        let {
        stepForm,
        amountStep
        } = this.state
        switch (buttonName) {
        case "prev":
        this.setState({
        stepForm: stepForm - 1
        })
        this.setState({
        isLastSection: false
        })
        break;
        case "next":
        this.setState({
        stepForm: stepForm + 1
        })
        if ((stepForm + 1) == (amountStep - 1)) {
        this.setState({
        isLastSection: true
        })
        }
        break;
        case "submit":
        this.handleSubmitForm();
        break;
        default:
        break;
        }
        }
        
    render() {
    //setState ***
    const {
        isShowAlert,
        colorAlert,
        messageAlert,
        colWidth,
        stepForm,
        amountStep,
        isLastSection,
        id,
create_at,
update_at,
personal_img,
first_name,
last_name,
birth_date,
have_car_cer,
married_status,
} = this.state
    
    //list options Select ***
    const optionsMarried_status=[
{ label: "โสด", value: "single" },
{ label: "แต่งงานแล้ว", value: "married" },
{ label: "หย่าร้าง", value: "divorce" },
]

    //steps ***
        const steps =[
"Section 1",
"Section 2",
]

    return (
    <StepperForm
        isStepper={true}
        stepForm={stepForm}
        steps={steps}
        amountStep={amountStep}
        isLastSection={isLastSection}
        onClickButtonForm={(e) => this.handleClickButtonForm(e)}
    >
    <SnackbarAlert isShowAlert={isShowAlert} colorAlert={colorAlert} messageAlert={messageAlert} onClose={this.handleCloseAlert} />
    {stepForm == 0 &&
                        <CRow>
 <CRow>
                <CCol
                 className={colWidth}
                  sm={4}
                  style={{ marginBottom: "20px" }}
                >
                <CRow>
                <ImageUploading
                multiple={false}
                value={personal_img}
                onChange={this.handleOnChangeInputImage("personal_img")}
                maxNumber={69}
                dataURLKey="data_url"
                >
                {({
                imageList,
                onImageUpload,
                onImageRemoveAll,
                onImageUpdate,
                onImageRemove,
                isDragging,
                dragProps,
                }) => (
                <div>
                <CRow>
                <CCol>
                <CFormLabel>
                {"รูปถ่าย"}
                <AsteriskPoint show={true} />
                </CFormLabel>
                </CCol>
                <CCol>
                <CButton
                onClick={onImageUpload}
                {...dragProps}
                color="light"
                >
                Browse
                </CButton>
                </CCol>
                </CRow>
                {imageList.map((image, index) => (
                <CRow key={index}>
                <CCol
                sm="10"
                style={{ marginTop: "20px", padding: 0 }}
                >
                <img
                src={image.data_url}
                alt=""
                height={150}
                width="100%"
                />
                </CCol>
                <CCol
                sm="2"
                style={{ marginTop: "20px", padding: 0 }}
                >
                <CButton
                className="btn-sm"
                color="danger"
                variant="outline"
                onClick={() => onImageRemove(index)}
                >
                X
                </CButton>
                </CCol>
                </CRow>
                ))}
                </div>
                )}
                </ImageUploading>
                </CRow>
                </CCol>
                </CRow>
                <CCol
                className={colWidth}
                sm={6}
                style={{ marginBottom: "20px" }}
                >
                <CFormLabel>
                {"ชื่อ"}
                <AsteriskPoint show={true} />
                </CFormLabel>
                <CFormInput
                type="text"
                id={"first_name"}
                required={true}
                maxLength={256}
                onChange={this.inputChange("first_name")}
                value={first_name}
                placeholder="กรอกชื่อจริง"
                />
                </CCol>
                <CCol
                className={colWidth}
                sm={6}
                style={{ marginBottom: "20px" }}
                >
                <CFormLabel>
                {"นามสกุล"}
                <AsteriskPoint show={true} />
                </CFormLabel>
                <CFormInput
                type="text"
                id={"last_name"}
                required={true}
                maxLength={256}
                onChange={this.inputChange("last_name")}
                value={last_name}
                placeholder="กรอกนามสกุล"
                />
                </CCol>
                <CCol
                className={colWidth}
                sm={6}
                style={{ marginBottom: "20px" }}
                >
                <CFormLabel>
                {"วันเกิด"}
                <AsteriskPoint show={true} />
                </CFormLabel>
                <DateInput
                id={"birth_date"}
                required={true}
                onChange={this.inputChangeDate("birth_date")}
                value={birth_date}
                placeholder="วว/ดด/ปป"
                />
                </CCol>
                </CRow>
                       }
{stepForm == 1 &&
                        <CRow>
<CCol
                className={colWidth}
                sm={12}
                style={{ marginTop: "10px", marginBottom: "20px" }}
                >
                <CFormLabel></CFormLabel>
                <CFormCheck
                id={"have_car_cer"}
                label={"มีใบขับขี่"}
                onChange={this.inputCheck("have_car_cer")}
                defaultChecked={have_car_cer}
                />
                </CCol>
                <CCol
                className={colWidth}
                sm={12}
                style={{ marginBottom: "20px" }}
                >
                <CFormLabel>
                {"สถานะสมรส"}
                <AsteriskPoint show={true} />
                </CFormLabel>
                <Select
                className="basic-single"
                classNamePrefix="select"
                isClearable={false}
                options={optionsMarried_status}
                required={true}
                defaultSingleValue = {married_status}
                onChange={this.handleChangeSingleOption("married_status")}
                placeholder=""
                />
                </CCol>
                
                </CRow>
                       }
</StepperForm >
    ) 
} 
    }
export default CustomersForm