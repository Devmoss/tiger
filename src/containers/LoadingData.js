import React from 'react';
import {
    CSpinner
} from "@coreui/react";
const LoadingData = (props) => {
    let {
        isLoaded,
        isDataEmpty
    } = props
    return (
        <div className="text-center">
            {!isLoaded &&
                (
                    <div style={{ marginTop: 20 }}>
                        <img src={require(`../assets/icons/icon_loading.png`).default} />
                        <div>
                        <CSpinner style={{ marginTop: 20 }} color="success" />
                        <h5 style={{ marginTop: 10 }} >กรุณารอสักครู่...</h5>
                        </div>
                    </div>
                )
            }
            {isDataEmpty &&
                (
                    <div style={{ marginTop: 20 }}>
                        <img src={require(`../assets/icons/icon_loading.png`).default} />
                        <h5 style={{ marginTop: 20 }}>ไม่พบข้อมูล</h5>
                    </div>
                )
            }
        </div>
    )
}
export default LoadingData
