import React, { useState } from 'react'
import { MapContainer, TileLayer, Marker, Popup, WMSTileLayer, LayersControl, useMapEvents, FeatureGroup, useMap } from 'react-leaflet';
import * as L from 'leaflet';
import {
  CCol,
  CProgress,
  CRow,
} from '@coreui/react'
// import 'proj4leaflet';
// import 'leaflet-easyprint';
// import 'leaflet-rotatedmarker';


// import { convertLatLngtoPoint, convertCoordinatetoLatLng, convertCoordinatetoLatLngMirror, convertCoordinatetoLatLngLand, convertCoordinatetoLatLngLandCenter, convertCoordinatetoLatLngBuild, mapRemoveLayer, convertCodePt4, refreshLayer } from '../utils'
// import { apiUrl, apiWMS, apiAerialPhotograph } from '../settings'
// import { convertArea } from '@turf/turf';
const axios = require("axios");


class Map extends React.Component {

  constructor(props) {

    super(props)
    // const latPosition = this.props.landCenterPolygon.lat;
    // const lngPosition = this.props.landCenterPolygon.lng;

    this.state = {
      position: [51.5, -0.09],
      landPolygon: '',
      markers: [],
      land: [],
      AddBuilding: false,
      hide: false,
      lat: '',
      long: '',
      MapZoom: true,
      Landlimit: true,

    }

    console.log(this.props);
  }


  refreshCountLayerInfo = () => {

  }

  async componentDidMount() {
    // _org_code = this.props.org_code;
    // landPolygon = this.props.landPolygon;
    // landCenterPolygon = this.props.landCenterPolygon;
    setInterval(this.refreshCountLayerInfo, 30000);
  }
  componentDidUpdate() {

  }


  render() {
    // let datalid = this.props.lid
    // let datalatlng = this.props.latlng
    // let dataorgmid = this.props.orgmid
    // let dataorgcode = this.props.Org_Code;
    // let landPolygon = this.props.landPolygon;
    // let landCenterPolygon = this.props.landCenterPolygon;
    // let scaleData = 0;
    // let position = this.state.position;//this.state.landCenterPolygon;
    // let minZoom = 5;
    // let maxZoom = 25;
    // let maxNativeZoom = 22;
    // let newPostion = [];
    // //const apiWMS = "http://192.168.1.99:8080/geoserver/microgravity/wms?";

    // ///===================
    // function pixelsToMetricDistance(pixelsDistance, thisMap) {
    //   var containerMidHeight = thisMap.getSize().y / 2,
    //     point1 = thisMap.containerPointToLatLng([0, containerMidHeight]),
    //     point2 = thisMap.containerPointToLatLng([pixelsDistance, containerMidHeight]);

    //   return point1.distanceTo(point2);
    // }

    // function getContainerMidHeightLatitude(thisMap) {
    //   var containerMidHeight = thisMap.getSize().y / 2,
    //     point = thisMap.containerPointToLatLng([0, containerMidHeight]);

    //   return point.lat.toFixed(6);
    // }


    // function GenFeatureGroupPolygon() {
    //   _countGenFeture++;
    //   console.log("_countGenFeture----*")
    //   console.log(_countGenFeture)
    //   //declair vairable
    //   _mapScale = "start";
    //   const [scaleMap, setScaleMap] = useState("");
    //   // const [viewland, SetViewland] = useState(false);

    //   const map = useMapEvents({

    //     click(event) {
    //       var layer = event.layer;
    //       // var geojson = layer;
    //       console.log(event);
    //       var pointXY = L.point(0, 0);
    //       console.log('containerPointToLatLng: ' + thisMap.containerPointToLatLng(pointXY))
    //     }
    //   });
    //   var thisMap = useMap();
    //   // alert(thisMap.getSize())
    //   var widthDisplay = thisMap.getSize().x//755;//755.905511811; //px = 20 cm  X
    //   var heightDisplay = thisMap.getSize().y//580;//566.929133858; //px = 15 cm  Y
    //   var widthBarScale = widthDisplay / 5; //px per bar  X
    //   var heightBarScale = heightDisplay / 5; //px per bar  Y
    //   var statusMapShow = false;//check map show polygon ok?
    //   var centerPosition = position;
    //   var getPixelOrigin = thisMap.getPixelOrigin();
    //   console.log("getPixelOrigin...");
    //   console.log(getPixelOrigin);
    //   var pointXY = L.point(0, 0);

    //   function isContrainDisplay(xLayer, yLayer) {//check ว่าแต่ละจุดอยู่ภายใน กรอบการแสดงผลที่ต้องการไหม
    //     var fixedPaddingMap = 40; //specific padding between border map and border polygon 
    //     var boundsX1 = fixedPaddingMap; //crop map
    //     var boundsX2 = widthDisplay - fixedPaddingMap;//crop map 
    //     var boundsY1 = fixedPaddingMap;
    //     var boundsY2 = heightDisplay - fixedPaddingMap;
    //     return ((xLayer >= boundsX1 && xLayer <= boundsX2) && (yLayer >= boundsY1 && yLayer <= boundsY2))
    //   }

    //   let getland = (scaleMapZoom) => {
    //     var countOutSideMap = 0; //count x or y from polygon outside map 
    //     var polygonIsInsideDisplay = false;
    //     console.log("genterate polygon !");
    //     console.log(thisMap.getSize());

    //     let landpoloygon = landPolygon;
    //     console.log(landpoloygon);
    //     if (landpoloygon[0] !== undefined) {
    //       let point = landpoloygon;//[0].coordinates;
    //       var letlngvalue = convertCoordinatetoLatLngLand(point);
    //       //latLngToLayerPoint
    //       for (let i = 0; i < letlngvalue.length; i++) {
    //         var latlng = L.latLng(letlngvalue[i][0], letlngvalue[i][1]);
    //         var xLayer = thisMap.latLngToContainerPoint(latlng).x;
    //         var yLayer = thisMap.latLngToContainerPoint(latlng).y;
    //         console.log(xLayer + " " + yLayer);
    //         if (xLayer >= 0 && yLayer >= 0) {
    //           if (!isContrainDisplay(xLayer, yLayer)) {
    //             console.log("Invalid!! " + xLayer + " " + yLayer);
    //             countOutSideMap++;
    //           }
    //         } else {
    //           countOutSideMap++;
    //         }
    //       }
    //       console.log(countOutSideMap);
    //       if (countOutSideMap == 0) {
    //         polygonIsInsideDisplay = true;
    //       }

    //       let polygon1 = L.polygon(letlngvalue, { color: 'red ' })
    //       thisMap.addLayer(polygon1);
    //       // let polygonLandaddselectCenter = polygon1.getBounds().getCenter()
    //       var polygonBounds = polygon1.getBounds();
    //       // // Getting the bounds of the map (you know how to do this)
    //       var mapBounds = thisMap.getBounds();
    //       console.log("ขอบเขตแผนที่ .... ")
    //       console.log(mapBounds)
    //       // // Now, determine if the polygon bounds are within the map bounds
    //       var containsMapBounds = mapBounds.contains(polygonBounds);
    //       console.log("is contains : ", containsMapBounds)

    //       // if (containsMapBounds&&polygonIsInsideDisplay) {
    //       if (polygonIsInsideDisplay && containsMapBounds) {
    //         console.log("Yeah! zoom success");
    //         statusMapShow = true;
    //         thisMap.setView(centerPosition, scaleMapZoom);
    //         // thisMap.invalidateSize();
    //       }
    //       else {
    //         thisMap.removeLayer(polygon1); //remove polyfon 
    //         console.log('ไม่อยู่ในขอบเขต');
    //         thisMap.setZoom(thisMap.getZoom() - 1); //zoom out 
    //       }
    //     }
    //   }
    //   thisMap.attributionControl.setPrefix('')
    //   thisMap.invalidateSize();
    //   getland(21) //ครั้งแรก Map ซูมเข้าหาระดับ 21 ก่อน 1:500 
    //   new SimpleMapScreenshoter().addTo(thisMap)


    //   // L.easyPrint({
    //   //   title: 'My awesome print button',
    //   //   position: 'bottomright',
    //   //   sizeModes: ['A4Portrait', 'A4Landscape'],
    //   //         // hideClasses: ['leaflet-control-zoom']
    //   // }).addTo(thisMap);

    //   // var printPlugin = L.easyPrint({
    //   //   hidden: true,
    //   //   sizeModes: ['A4Portrait']
    //   // }).addTo(thisMap);
    //   // printPlugin.printMap('A4Portait', 'MyFileName');

    //   function getScaleThaiMapFromZoomLevel(thisMap) {
    //     switch (thisMap) {
    //       case 21:
    //         return '1:500';
    //       case 20:
    //         return '1:1000';
    //       case 19:
    //         return '1:2000';
    //       case 18:
    //         return '1:4000';
    //       case 17:
    //         return '1:10000';
    //       case 16:
    //         return '1:20000';
    //       default:
    //         return '1:null';
    //     }
    //   }
    //   function computeScale() {
    //     var mapZoom = thisMap.getZoom(),
    //       latitude = getContainerMidHeightLatitude(thisMap),
    //       distance = pixelsToMetricDistance(100, thisMap).toFixed(2);
    //     if (!statusMapShow && mapZoom >= minZoom) {//if this map not contrain polygon 
    //       console.log("TT map not contrain")
    //       getland(mapZoom)
    //     }
    //     var mapScaleData = "มาตราส่วน " + getScaleThaiMapFromZoomLevel(mapZoom);

    //     var mapBounds = thisMap.getBounds();
    //     // console.log("ขอบเขตแผนที่ จากระดับการซูม.... ")
    //     // console.log(mapBounds)
    //     console.log("At Zoom : " + mapZoom);
    //     return mapZoom;
    //   }

    //   thisMap.on('moveend', computeScale); // Also fires when zoom changes.
    //   var mapZoom = computeScale();


    //   return (
    //     <div>
    //       {/* <p id="test1">{mapScaleData}</p> */}
    //     </div>
    //   );

    // }
    const minZoom = 10
    const maxZoom = 22
    const maxNativeZoom = 1
    return (
      <MapContainer center={[13.835649, 100.036147]} zoom={13} scrollWheelZoom={false}
        style={{ height: "100vh" }}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png' />
        {/* <Marker position={position}>
          <Popup>
            <span>A pretty CSS3 popup. <br /> Easily customizable.</span>
          </Popup>
        </Marker> */}
      </MapContainer>
    )
  }

}


export default Map