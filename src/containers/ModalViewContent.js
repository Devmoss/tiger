import React, { useState } from 'react';
import {
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CModal,
    CTooltip,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
} from "@coreui/react";
import ConfirmDialog from './ConfirmDialog'
const ModalViewContent = (props) => {
    //create at 31/08/64 by dmoss
    var title = props.title
    var show = props.show
    var showDialogExit = (props.showDialogExit != undefined) ? props.showDialogExit : false
    var isPopupView = props.isPopupView
    var sizeModal = props.sizeModal ? props.sizeModal : "lg"
    var childrenData = props.children
    var handleChangViewForm = props.onChangeViewForm
    var handleClose = props.onClose
    const [isShowExitDialog, setShowExitDialog] = useState(false)

    function handleCloseExitDialog() {
        setShowExitDialog(false)
    }

    function handleShowExitDialog() {
        if (showDialogExit) {
            setShowExitDialog(true)
        } else {
            handleClose()
        }
    }

    function handleConfirmExit() {
        setShowExitDialog(false)
        handleClose()
    }

    return (
        <>
            {show &&
                (
                    <>
                        {isPopupView ?
                            (
                                <>
                                    {showDialogExit &&
                                        <ConfirmDialog
                                            show={isShowExitDialog}
                                            title={"กำลังออกจากหน้านี้"}
                                            content={"คุณต้องการออกโดยไม่บันทึก ใช่หรือไม่?"}
                                            onConfirm={handleConfirmExit}
                                            onClose={handleCloseExitDialog} />
                                    }
                                    <CModal
                                        size={sizeModal}
                                        visible
                                    >
                                        <CModalHeader>
                                            <CCol>
                                                <h4>{title}</h4>
                                            </CCol>
                                            <CCol style={{ textAlign: 'right' }}>
                                                <CTooltip
                                                    content="แสดงแบบด้านข้าง"
                                                    placement="bottom"
                                                >
                                                    <button className="btn" onClick={() => handleChangViewForm()}>
                                                        <img height="25" src={require('../assets/icons/icon_sidebar.png').default} />
                                                    </button>
                                                </CTooltip>
                                                <button className="btn btn-close" aria-label="Close" onClick={() => handleShowExitDialog()}></button>
                                            </CCol>
                                        </CModalHeader>
                                        <CModalBody>
                                            {childrenData}
                                        </CModalBody>
                                    </CModal>
                                </>
                            )
                            :
                            (
                                <>
                                    {showDialogExit &&
                                        <ConfirmDialog
                                            show={isShowExitDialog}
                                            title={"กำลังออกจากหน้านี้"}
                                            content={"คุณต้องการออกโดยไม่บันทึก ใช่หรือไม่?"}
                                            onConfirm={handleConfirmExit}
                                            onClose={handleCloseExitDialog} />
                                    }
                                    < div id="sidebar-view">
                                        <div id="sidebar-form-view">
                                            <CCard className="mb-4">
                                                <CCardHeader>
                                                    <CRow>
                                                        <CCol style={{ textAlign: 'right' }}>
                                                            <CTooltip
                                                                content="แสดงแบบ Popup"
                                                                placement="bottom"
                                                            >
                                                                <button className="btn" onClick={() => handleChangViewForm()}>
                                                                    <img height="25" src={require('../assets/icons/icon_popup.png').default} />
                                                                </button>
                                                            </CTooltip>
                                                            <button className="btn btn-close" aria-label="Close" onClick={() => handleShowExitDialog()}></button>
                                                        </CCol>
                                                    </CRow>

                                                    <CRow>
                                                        <CCol style={{ marginTop: -10 }}>
                                                            <h4>{title}</h4>
                                                        </CCol>
                                                    </CRow>
                                                </CCardHeader>
                                                <div style={{ height: 745, width: '100%', overflowY: 'auto', overflowX: 'auto' }}>
                                                    <CCardBody>
                                                        {childrenData}
                                                    </CCardBody>
                                                </div>
                                            </CCard>
                                        </div>
                                    </div>
                                </>
                            )
                        }
                    </>
                )
            }
        </>
    )


}
export default ModalViewContent
