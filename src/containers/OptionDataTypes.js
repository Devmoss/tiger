import React from 'react'

const OptionDataTypes = [
    // { label: 'ID', value: 'ID', icon: "icon_single_line_text.png" },
    { label: 'Single line text', value: 'SingleLineText', icon: "icon_single_line_text.png" },
    { label: 'Multi line text', value: 'MultiLineText', icon: "icon_multi_line_text.png" },
    { label: 'Email', value: 'Email', icon: "icon_email.png" },
    { label: 'Whole Number', value: 'WholeNumber', icon: "icon_number.png" },
    { label: 'Decimal Number', value: 'DecimalNumber', icon: "icon_decimal.png" },
    { label: 'Date Time', value: 'DateTime', icon: "icon_date_time.png" },
    { label: 'Boolean', value: 'Boolean', icon: "icon_boolean.png" },
    { label: 'Json', value: 'Json', icon: "icon_json.png"},
    { label: 'Single Select Option Set', value: 'OptionSet' , icon: "icon_single_select.png"},
    { label: 'Multi Select Option Set', value: 'MultiSelectOptionSet', icon: "icon_multi_select.png" },
    { label: 'Lookup From Entity', value: 'LookupFromEntity', icon: "icon_lookup.png" },
    { label: 'Relation to Entity', value: 'RelationToEntity' , icon: "icon_related_entity.png"},
    { label: 'Geographic Location', value: 'GeographicLocation', icon: "icon_geo_location.png" },
    { label: 'Create At', value: 'CreateAt', icon: "icon_create_at.png" },
    { label: 'Update At', value: 'UpdateAt' , icon: "icon_update_at.png"},
    { label: 'Upload Image', value: 'UploadImage', icon: "icon_upload_image.png" },
];

export default OptionDataTypes
